// Code generated by go-bindata.
// sources:
// templates/README.md
// templates/identities_registry.gotpl
// templates/model.gotpl
// templates/relationships_registry.gotpl
// DO NOT EDIT!

package static

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz) // #nosec
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

func (fi bindataFileInfo) Name() string {
	return fi.name
}
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}
func (fi bindataFileInfo) IsDir() bool {
	return false
}
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _templatesReadmeMd = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x04\xc0\xcb\x11\x02\x21\x10\x45\xd1\x7d\x47\xf1\x2c\x53\x22\x81\xc6\xbe\x0a\xc5\x47\x6a\x60\x33\xd9\xcf\x79\x2b\x31\x56\xf7\xc3\x36\x4b\x85\x8d\xbe\xb5\xa3\x09\xa1\xf3\x57\x46\x8c\x4c\x04\xa1\x3a\x75\x0a\xca\x75\xfa\x75\xbf\xcc\x24\x69\x78\x43\xcb\x3f\xcd\x7f\xd8\x13\x00\x00\xff\xff\xaa\x97\xff\x85\x4d\x00\x00\x00")

func templatesReadmeMdBytes() ([]byte, error) {
	return bindataRead(
		_templatesReadmeMd,
		"templates/README.md",
	)
}

func templatesReadmeMd() (*asset, error) {
	bytes, err := templatesReadmeMdBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "templates/README.md", size: 77, mode: os.FileMode(420), modTime: time.Unix(1625075149, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _templatesIdentities_registryGotpl = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xdc\x58\x4b\x6b\xe4\x38\x10\xbe\xfb\x57\xd4\x86\xb0\xd8\x90\xb8\x2f\xcb\x1e\xb2\xe4\x10\x32\x19\x68\xd8\x64\x42\x32\xec\xa5\x09\x8b\xe2\x2e\x3b\x62\x65\xc9\x48\x72\x66\x8c\xf1\x7f\x5f\x24\xcb\x0f\xb9\xdd\x9d\xce\x63\x7a\x60\x72\x49\xdb\x56\xd5\x57\xf5\x95\xea\xd3\x63\xb1\x80\x4b\xb1\x46\xc8\x90\xa3\x24\x1a\xd7\xf0\x58\x01\x32\xcc\x90\xc7\xf0\xe9\x0b\xdc\x7c\xf9\x0a\x57\x9f\x96\x5f\xe3\x60\xb1\x80\x7b\x51\xca\x04\xcf\x20\x13\x31\x29\x84\x44\x2d\x62\x2a\x16\xc8\x30\x47\xae\x09\x83\x50\x63\x5e\x30\xa2\x51\x2d\xe8\x1a\xb9\xa6\x9a\xa2\xfa\x57\x62\x46\x95\x96\x55\x9c\x09\x5d\xb0\x28\x08\x0a\x92\xfc\x47\x32\x84\xba\x86\xf8\x1e\x75\x7c\x29\x78\x4a\xb3\x52\x12\x4d\x05\x8f\x6f\x48\x8e\xd0\x34\x41\x40\xf3\x42\x48\x0d\x47\x5b\xd0\x8e\x82\xe0\x99\x48\x08\x03\x00\x00\x07\x57\x19\x63\x75\x4d\x0a\x38\x87\x9c\x14\x2b\xa5\x25\xe5\xd9\x43\x6f\x13\x2f\xdd\x38\xa8\xad\x99\xf9\xab\xeb\x53\x90\x84\x67\xd8\x06\x73\x5f\x60\x42\x53\x9a\xd8\x60\x94\x09\x64\x18\x08\x34\x05\xf5\x24\x4a\xb6\xbe\xb3\x39\xa1\xf4\x46\x43\x0c\xc7\xf1\x6d\xf9\xc8\x68\x72\x6d\x38\xf5\x6c\x4f\xe1\x78\x08\x11\xce\xce\x21\x36\x63\x58\x7c\x35\xbc\x3c\x1d\x19\x1c\xd5\xb5\x1b\x70\x87\x4a\x9b\xcf\x4d\x73\x74\x66\x62\x18\xbb\x69\x9a\x2e\xa1\x13\x0f\x0a\xf9\x7a\x8a\x3e\x7a\xd5\x04\x1e\x67\x09\xd1\x98\x09\x49\x7f\x41\xe2\xec\x7c\xfd\x21\xe4\x11\x46\x89\xfa\x81\x8c\x9d\x1e\x90\xb2\xba\x76\x51\x1d\xd3\x13\x38\xb6\x99\x8d\x8c\x2e\xda\x4c\xc1\xe7\xb8\x1b\xf7\x71\xc4\xee\x98\xa8\x7c\x8d\xdf\x67\xb8\x5e\x3d\xac\x1e\xda\x9f\x07\xe7\xd8\x30\x30\xe9\xcf\x9e\x0a\x9a\x02\x43\x0e\xf1\xb2\x0d\x1b\x9a\x66\x14\x9e\x1f\xa2\x25\x3c\x11\x79\x21\x4a\xbe\x36\x9c\x2b\x21\x75\x67\x37\x72\xe0\x9b\x8f\x1d\xf4\xc6\x4d\x63\x63\x32\xff\x4f\x06\x1a\xa1\xd9\x59\x82\xe6\xa4\xae\x01\x99\x32\xc1\x73\xca\x4e\xde\x58\xa5\x28\x30\xab\x83\xa5\xe3\x1f\x94\xca\x50\x27\x51\x97\x92\x2b\xd0\x4f\x08\x49\x29\x25\x72\x0d\xcf\xee\x9b\x48\xed\xeb\xdc\xd2\x17\xa4\x25\x4f\x3c\xdb\x30\x82\x94\x09\xa2\xff\xfc\x03\x6a\xe7\xa7\x5f\x2a\x2e\x6e\x97\x4b\x9e\x8a\xb8\x83\x31\x19\x06\x81\xae\x0a\xe7\xee\x9a\x70\x92\xa1\x04\xa5\x65\x99\xe8\xba\x09\xac\xfb\x30\xf5\xbe\x46\xd0\xcd\xcf\xcf\x52\xe4\xa6\x76\x21\x37\x05\x6c\xe7\x52\x04\xb3\x3d\x6c\x53\x75\xd1\x4c\x57\x9b\x95\x31\x7f\x08\xf6\x41\xbb\x6c\xc5\xb6\x0a\x9d\xea\x56\xaf\x47\xf5\xf4\x7a\xd5\xf9\xd9\x0f\xde\x36\x73\xd8\xb6\xee\xde\xc0\x83\xd6\xad\xec\xcf\x3d\xa1\x78\x15\x12\x3e\xe4\x17\xd2\x19\xa4\xa8\x83\xa2\x29\x50\x38\x87\x34\xde\x28\x0d\xe1\x55\xf4\x17\xfc\x46\xe3\xa5\xba\xca\x0b\x5d\x85\xd1\xa8\x9d\x3a\x6a\x3c\xb9\x98\x73\xd5\xf3\xfe\x6a\x77\xee\x9d\xef\xce\xf1\xc8\xab\xe8\x05\x2e\x52\x4a\x1e\x19\x86\x5d\xed\x66\x29\x98\xbe\x6b\x6d\x3a\x66\xd4\x37\xaa\x93\xa7\xbe\xfa\x2e\xda\x5e\xb3\x77\xa8\xdc\x9b\x15\x2e\x21\xaa\xdd\x9d\x6d\x2c\x1b\x83\xb4\x9f\x4d\x49\xbb\xc1\x6f\x5b\x4c\xc2\x28\x98\x91\x8d\xc9\xe3\x1a\x53\x52\x32\xbd\xe1\x96\x53\xe6\xaa\xb1\x8d\xe8\xfb\x82\x48\x85\x6f\xa1\x7b\xd3\xf2\x27\x92\xee\x0c\xb9\xd0\x1d\x89\x4b\x75\x27\x84\x7e\x6f\x51\xda\x24\xdf\x53\x9a\x0f\xab\x94\x5b\xd0\x76\x97\xc7\x5b\xd5\x3d\xfd\xeb\xb7\x01\xab\xce\x81\x3d\x2c\xbc\x24\x47\x6d\x65\x4d\xd7\xde\x5b\xb7\x9e\x2a\xed\xee\xbd\x49\xef\xbb\xd9\x35\x91\x82\x56\xe7\xa2\xfd\x94\xe0\x85\xe4\xe7\xc3\x51\x1f\x36\x2d\xb7\xce\xae\xc3\x6a\xc5\xef\x73\x06\xb7\xac\x94\x84\x41\xd3\xfc\x4d\x95\x59\xba\x0f\x38\x31\x37\x85\x60\xef\x3a\xcd\x98\xfe\x72\xd5\xda\x2e\x21\x3f\xb1\x66\x1e\xe5\xaf\xea\x6e\xb5\xb3\xbd\xd5\xab\xfb\xfb\x0e\x59\x5b\xbe\x27\x5a\xa8\x70\x8c\xea\x7d\xb9\x73\xf7\x20\x13\x74\x39\x37\xa6\xc3\x82\x0d\xb0\x0b\xc6\x96\xfd\xdd\x4a\x68\xe4\x72\xeb\x91\xd3\x01\x4c\x2c\x8c\xeb\x67\x22\x21\x77\x5b\xe5\x73\x0f\xc0\x6c\x99\xcd\x5e\xde\x7d\x1c\x6f\xe3\xed\xb0\x51\x72\xd7\x23\xb3\x6e\x1b\xdf\x3e\x79\x1c\x8c\x87\x0d\x1b\xfa\x0e\xbd\x09\x2c\x9e\x17\x63\x8f\x4a\x18\x03\xfc\x4e\x95\x36\x8b\xc1\x70\xa3\xe4\xc0\xf6\x64\x62\x4c\xc5\xdc\x90\xc3\x1f\xcf\x77\x76\xdf\xeb\xee\x22\x1a\xc7\x9e\xdd\xa0\x7f\x16\xb2\xcf\x7b\x4c\xa1\x29\x9e\xdb\xc3\x43\x2a\xa4\x7d\xce\xe8\x33\x0e\x47\x8a\x9e\xd1\xa9\x9f\x97\x96\x6a\x7f\xa1\xde\x26\x78\x7b\xd0\x7a\x58\x45\xeb\x02\xaf\xdf\x7b\xf7\xb1\xfb\x74\xbd\x87\xfe\xf9\x87\x0c\xa3\x7a\x4d\xf0\x7f\x00\x00\x00\xff\xff\xb0\x32\x72\xa5\x94\x15\x00\x00")

func templatesIdentities_registryGotplBytes() ([]byte, error) {
	return bindataRead(
		_templatesIdentities_registryGotpl,
		"templates/identities_registry.gotpl",
	)
}

func templatesIdentities_registryGotpl() (*asset, error) {
	bytes, err := templatesIdentities_registryGotplBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "templates/identities_registry.gotpl", size: 5524, mode: os.FileMode(420), modTime: time.Unix(1661450781, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _templatesModelGotpl = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xec\x3d\x6b\x73\xdb\x38\x92\x9f\xa3\x5f\x81\x51\x79\x12\x2a\x2b\x53\xf9\xac\x59\x6f\x55\x62\x27\x73\xae\xcb\xc3\x15\x67\x67\xab\x2e\x95\xda\xc0\x24\x28\x63\x42\x01\x1c\x00\xb4\xe3\x53\xf1\xbf\x5f\xe1\x41\x12\xe0\x4b\xa0\x2c\x65\x3c\xb7\xf6\x87\x19\x1b\x8f\x46\xbf\xd0\xe8\x6e\x80\x9d\xc5\x02\x9c\xd2\x18\x81\x15\x22\x88\x41\x81\x62\x70\x75\x07\x50\x8a\x56\x88\x84\xe0\xec\x03\x78\xff\xe1\x13\x78\x7d\x76\xfe\x29\x9c\x2c\x16\xe0\x92\xe6\x2c\x42\x4b\xb0\xa2\x21\xcc\x28\x43\x82\x86\x98\x2e\x50\x8a\xd6\x88\x08\x98\x82\x40\xa0\x75\x96\x42\x81\xf8\x62\x4d\x63\x94\x86\x2b\x2a\xb2\x74\x36\x99\x64\x30\xfa\x06\x57\x08\x6c\x36\x20\xbc\x44\x22\x3c\xa5\x24\xc1\xab\x9c\x41\x81\x29\x09\xdf\xc3\x35\x02\x45\x31\x99\xe0\x75\x46\x99\x00\xc1\x04\x00\x00\xa6\xc9\x5a\x4c\xf5\x6f\x3d\xeb\x95\xbd\x58\x5c\xe7\x57\x61\x44\xd7\x8b\x55\x4a\xaf\x60\xca\xf1\x8a\x2c\xd6\x2b\xba\xb8\xe2\x94\xb4\x07\xad\xb1\x88\xae\x51\x9a\x5e\x2f\x22\x9a\xdd\x71\xc1\xf2\x48\xe4\x0c\xe9\x81\x9b\xcd\x31\x60\x90\xac\x10\x08\x2f\x33\x14\x85\x9f\xee\x32\x74\xc1\xe8\x0d\x8e\x11\xe3\x12\x49\x05\x4d\xd2\x01\x8a\xa2\x9e\x82\x48\x0c\x8e\x4d\x6f\x13\xc4\x6f\x30\xc5\xb1\xa2\xd4\x13\x50\x51\x4c\x66\x93\xc9\x66\x03\x8e\x14\x2f\xc5\x6f\x88\x71\x4c\x09\x58\x9e\x18\x88\x6f\x55\xf3\x4b\x21\x18\xbe\xca\x05\xe2\xe5\x00\xc9\xc3\xcd\xc6\x2c\x7e\x84\xe7\xe0\x08\x91\x7c\x2d\xe7\x5d\xe5\x38\x8d\x5f\x93\x7c\xcd\x35\x88\x26\xe8\xa2\x90\xf2\x95\x4b\xca\x19\x8a\x6a\x50\x14\x80\xa1\x8c\x21\x8e\x88\xe0\x40\x5c\x23\x90\x51\xce\xf1\x55\x8a\xc0\x0d\x4c\x73\xc4\x41\x42\x19\x80\x25\x16\x8a\x18\x3d\xbd\xc2\xcc\x48\x76\x1a\x4e\x84\x84\xd8\x82\xcf\x05\xc3\x64\x35\x99\x44\x94\xf0\x52\xee\x35\xfb\x8e\x08\x5c\xa3\x39\x38\x52\xab\x49\x2a\xf4\xe4\xdf\xf4\xe2\x86\x85\x06\x6d\xa2\x57\x6a\x62\xac\xa7\xca\x01\xfa\xb7\xa2\x08\xcd\x22\xf5\x94\x16\x56\x27\x9a\x94\x72\x46\x29\x9c\x5a\x36\xf5\xef\x13\x89\x2d\x4e\x00\xa1\xc2\xc8\xe6\x9d\x52\xfc\x33\x24\x60\x74\x8d\xe2\x9a\xb1\x76\xef\x6b\x22\xb0\xb8\x33\xcc\x39\x8f\x91\xfa\xb3\x89\x7a\xd5\x4e\x13\xf5\x37\xbd\xfa\x1d\x45\x22\x9c\xdc\x40\xe6\x07\xef\x04\x54\x3b\x25\xac\x1a\x37\x8a\x18\x39\x74\x09\x2a\x0d\xb4\x40\x7d\x44\x5c\xc8\xde\xa2\x98\xce\xd5\xd0\x53\x28\xd0\x8a\xb2\xbb\x65\xd7\x50\x65\x0f\x4a\x21\xeb\xf1\x17\x7a\xab\x2f\xdb\xa0\x4d\x4f\x3d\x92\xe1\x1b\x28\xe4\xc8\xe6\x40\xdd\x51\x14\xf3\x49\x31\x92\xd7\x9b\x4d\xd7\x88\x73\xfe\x91\x52\xb1\x4d\x16\x17\x69\xce\x60\x0a\x8a\xe2\x2d\xe6\xc2\x96\x06\x04\xa9\x6c\xa1\x89\xc7\xdc\x4a\xd1\x7d\xd6\xf8\xfc\xe5\x79\xef\x48\x49\xf0\x62\x01\x2c\xed\x10\x39\x23\x5a\x35\x70\xa7\x6a\x70\x80\x89\xfa\x53\x62\x1b\x4e\x92\x9c\x44\x20\xa0\x9e\xb8\xcc\xaa\x95\x82\x59\xb7\xde\x28\x99\x69\x2c\xfa\x61\xd6\xea\x37\xd1\xf8\x9f\xd2\xac\xc6\x1d\x82\x8c\x62\x22\x10\x03\x82\x02\x08\xa4\xf9\x55\x08\xfb\xa1\x38\x9e\x24\xb9\x78\x07\x39\x09\x86\x57\x29\xe2\x25\x4d\x0a\x8d\xe5\x09\x80\x59\x86\x48\x1c\xf8\x01\xdf\x14\x73\x40\xc3\x30\x9c\xd9\x6c\x79\x2a\x41\x19\xc2\x5f\x2a\x68\x06\x28\x77\xc4\x24\xa8\xfa\x13\x02\x82\x6e\xf5\xea\x46\x8e\x87\xe2\x83\xc6\x25\x28\xd7\x0f\xc3\xb0\x9b\x25\x5b\x59\x45\x73\x71\x4f\x4e\xc9\x23\xe3\xdf\x73\xc9\x0a\x09\x48\xdb\xf9\x12\x2f\x6d\x9b\xca\x75\xaa\x65\x68\x2e\xd4\x84\x30\x18\xda\x2d\x33\x0d\xbf\x70\xf4\x94\xe6\xc2\x88\x43\xed\xb7\x88\x92\x1b\xc4\x84\x2d\x0d\xa5\x89\xa4\x8f\xee\xdd\xd8\x2d\xff\xdb\xaf\x76\x0a\x13\x97\x9f\x6b\xf8\x0d\x05\x03\xc3\xe7\x20\x45\x24\xa0\xb3\x9a\x85\x58\x4e\x7b\xf1\x0b\xc0\xe0\xef\xa6\xef\x17\x80\xff\xf6\x37\x97\x85\x9f\xf1\x17\x70\x02\xe8\x67\xfc\x65\x90\x35\x67\x28\x81\x79\x2a\x3e\xb0\x18\x31\xc7\xcc\xc4\xba\x03\x50\xd9\x83\xc9\x0a\x24\x18\xa5\x31\x2f\xb5\x35\xa2\x44\x20\xb2\x03\x7f\xec\x05\x83\x19\xf8\xfc\x45\xbb\x01\x0d\x1b\x53\x36\xd7\x24\x55\xae\x8d\x5e\xc5\xc1\xbb\x74\xbe\x1c\xaf\x6a\x6e\x4f\x35\xa7\x88\xe6\x84\xa6\xfc\x13\xbd\xcc\x20\xe3\xc8\xa1\xda\xd3\x76\x1b\x5d\x42\xb1\xd4\x20\x0d\xc6\x77\xfb\x2e\x16\xe0\x43\xdb\x62\x83\x5b\x9c\xa6\x80\x92\xf4\x4e\x71\x16\x9a\xae\x15\xbe\x41\xc4\x70\x3e\x04\xef\xa9\xfe\x15\xac\x11\x24\x1c\x48\x3d\x61\xc8\x34\x71\xb4\x83\x2c\x4a\x16\x04\x46\xb6\x61\x18\x6a\xb6\xfb\xda\x02\xa5\xbb\x63\xe8\xbf\xb7\x32\x87\x0d\x9c\xa5\x6d\x09\x83\xe7\x5b\x70\x00\x45\x31\x6c\x21\x4a\x57\xd8\xd6\x85\x1b\xd3\x76\x5f\x8d\x37\xb0\x83\x19\xc0\x44\x74\x9d\xa5\x48\x84\x2f\x2f\xce\xcf\x49\x42\x43\xcb\x25\xd7\xee\xbc\x51\x5c\x2b\x3a\x50\xbf\x1f\x61\xfe\x9a\x44\xec\x2e\x13\x52\x2c\x92\x85\x09\x4c\x39\xf2\xf0\x38\x9b\x9e\xa6\x8a\xd5\x24\x8d\xb0\x39\xad\xf4\x06\xcb\xf5\xf5\xf6\x53\xe3\x4f\xe9\x5a\x6a\xc7\x9b\x14\xae\xb8\xbb\xd4\x77\x81\x88\xa4\x80\x5b\xa8\x34\x08\x18\x76\x92\x4c\x64\x90\x47\x92\x55\xcd\x68\xaa\x8e\x7a\x9a\x31\xcc\x71\x51\x18\x0f\x31\x34\x8c\x51\x3e\x61\x07\xaf\x4e\x80\x60\xca\xb5\xb7\x50\xda\x6c\x54\x28\xf3\x89\xbe\x51\x9b\xe9\x48\xca\xc4\x70\xb4\x89\xbd\x92\x9e\x42\xba\x5c\x5a\x4a\xf5\xeb\xef\x9c\x92\xe5\xf4\x78\x0a\xd6\x7c\x25\x03\x5e\xf5\xfb\x95\x6a\xfc\xb7\x62\x99\xd1\xa6\xe9\x57\xa3\x71\xef\xd1\xed\x16\x31\x95\x6e\x93\x74\x14\xfa\x0f\x3f\x89\x93\xd2\xc8\x2d\x00\x83\xd9\x30\x90\x86\x62\x3e\x1d\x1a\x5b\xef\x4d\x9b\x11\xcb\x01\x6d\x9e\x4f\x2c\x6b\x7c\x6c\x07\xa9\x92\xef\x52\x7f\x39\x65\x56\x50\x0b\x82\x2d\x02\x9f\x39\x46\xdf\x88\x9e\x5f\xd3\x3c\x8d\xff\xc5\xb0\x40\xe7\x04\x0b\x0c\x53\xfc\xbf\x88\x49\x71\xaa\xa8\x57\x2e\x15\x9e\x96\xa6\x5b\xd1\xd2\xd0\xa2\xa3\xf0\x22\xbf\x4a\x71\x24\xc9\x02\x0d\xf8\x47\x98\x60\x65\xf4\x6e\x3b\xe0\x23\xe1\xac\xd2\x9c\x8b\x13\x33\xdd\x69\xef\x6a\x3b\xb6\x8f\x2a\xbf\x26\x63\x2a\x7c\x42\xd1\xce\x80\xa2\x2f\xd6\x2c\xed\xdc\xa0\xda\xec\x29\x74\x00\x4e\xec\xd0\xb4\x76\xbe\x84\x25\xb8\xe1\xc4\xe8\x28\xde\xa1\xeb\x19\x07\x39\xc1\x7f\xe4\x65\x20\x25\xe7\x8c\xa4\x55\x4e\x09\x66\xc0\x75\x5c\x74\xf0\xa9\xe7\x5a\xd8\x94\x5a\x5a\x9e\x38\x61\xb5\x40\x3d\xa8\xa1\x93\x56\x26\x09\x49\x0b\xd4\x00\x31\x6d\xe5\x8b\x76\x61\xd8\x25\x12\x16\x96\x1c\x89\xc3\x30\xcc\x59\x26\xc0\x31\x28\xfd\x0b\x3f\xae\xf9\xb1\x0b\x9c\x00\x1c\x0f\x33\x65\xb1\x00\xbf\x22\xf1\xea\xf2\xc3\x7b\x80\xd7\x99\x56\x53\x4d\xb1\xb4\xd1\x60\x0d\x19\xbf\x86\xa9\x14\xa7\x0a\x51\x13\x18\x21\xe5\xaa\x7d\xba\xc6\x1c\x60\x0e\x72\xae\x7d\x3d\xc1\x20\xe1\x19\x64\x88\x08\xed\xaa\x49\x44\xc0\xf9\x99\xec\x7b\x47\xc9\x8a\x9e\xbd\x3a\x3f\x03\x90\x83\x0f\x57\x28\x12\xe7\x67\xde\x8c\x32\xd8\x05\x33\x10\x54\x18\xc8\xe0\x09\x31\x46\x59\xc5\x2e\x9c\x00\x0a\x4e\x4e\x00\xc1\xa9\xe5\x20\x19\xc5\x20\x38\x9d\xcb\xff\xd8\x8e\x0e\x97\x06\xeb\xe9\x5a\x62\x56\x9b\xd2\x41\xd3\x5e\x2a\x9f\xff\xb9\x3b\xb1\xac\x5c\x78\x29\x28\x43\x31\x68\xb4\x5a\xa2\x35\x3d\x92\x12\x25\xdc\x96\x30\x7f\x3a\x01\xd3\xa9\x45\x1d\xef\x1e\x76\xa2\x24\x17\x6a\x5f\xfa\x3c\xfe\x2f\xf4\x3d\xe8\x06\x58\x3a\x7e\xce\x9e\x32\x58\xf4\xc2\xee\x06\xd5\xd4\xb1\xe1\x3f\xed\x4d\xcb\xb5\x64\xb4\x26\x5e\x3e\x68\x4d\x34\xd8\x05\x0c\xde\x6a\x16\x7f\x84\xb7\x33\xad\x87\xbe\x6a\xb8\x0f\x0d\xc4\x89\x5c\x53\xe7\x09\x6e\xc3\x7f\x12\xc3\x98\x80\xcf\x7e\x51\x1d\x3f\xf5\x2c\x8f\x18\x73\x04\x7e\x60\x3d\xee\x51\xe2\x93\x1e\xd5\x0a\xa5\x9e\xce\x3a\x75\x71\x24\xa4\xdd\x75\xd1\x28\xa2\xdf\x39\xd1\x15\x18\x5d\x43\x16\x47\x34\x46\x71\x33\x44\xd2\xf7\x3e\xbe\x8a\xb6\x73\x5c\xd4\x30\xec\xaf\x52\x74\x83\x54\xfa\xbe\xb9\xa1\x64\x47\x78\x9a\x42\xce\xb5\xcc\xce\xeb\x1d\xe5\x89\x63\x05\xbb\x75\xde\x97\xa7\x71\x7f\xd0\x34\xf5\xe7\x72\x6f\x16\xa6\xcc\x3d\xf7\x64\x63\xbc\xe8\x50\x84\x3c\x88\xb4\xcb\x48\x37\xc5\x76\x0e\xca\x5e\x1e\x31\x9c\x89\xfa\xd6\xea\x8c\x46\x6e\xda\x8a\x46\xb9\xf2\x41\xd5\x98\x84\x32\xcb\x93\xf1\x15\xfa\x19\x8d\xfa\xc4\xfd\x55\x12\xc5\xa3\x57\x30\xfa\x26\x70\xf4\x8d\x0f\x60\xf7\xd5\xb9\xbf\x18\x49\xba\xaf\xad\x56\x38\xf6\x21\x9b\xac\x45\x78\x99\x31\x4c\x44\x12\x4c\xff\xfe\x33\x5f\xfe\xcc\xff\x31\x9d\x03\x1a\xd6\x2e\xbb\xba\x7e\xad\x9b\xb4\x67\x3b\x6b\x89\xca\xd3\x88\x56\x32\xd3\x81\xd8\xaf\xe6\x5a\xf9\x57\x24\x04\x62\x20\x6c\x85\x57\xda\x2b\xeb\x34\x7b\xcd\xa4\x5c\x6b\x80\x31\x39\x0c\x45\x08\xdf\x34\x3d\xd2\xa3\x61\x4f\xab\x0b\x60\x30\x73\xd7\x29\xef\x03\x5d\x96\xf6\xf8\x05\x8d\x5c\x4d\x9b\x05\x97\x03\x2c\xb8\xec\x61\x41\xe5\x94\x67\x8c\x66\x88\xc9\x60\xca\x83\x11\x20\xe7\x52\x13\xea\xec\xa1\x72\xe9\xfd\xd9\xd3\x83\x8d\xca\xf7\xbf\xaf\x2f\x4d\x5b\x8c\xaa\x7c\xd4\xde\x73\xcc\x82\xd0\xd8\x1a\xcd\x9d\x01\x49\x0c\x82\xbe\xed\x31\x6b\x77\xe9\x2b\xbe\x99\xe1\x67\x67\x62\x97\xeb\xa6\xee\x03\x4b\xb9\x57\xe5\x78\x14\x97\xb7\x03\x7b\xcd\xc9\x6e\xd9\xc9\x5e\xa9\x58\x3d\xc4\x4e\xc8\x5a\x1e\x59\x8a\x88\x99\x3c\x93\xbe\xd9\x0b\xcb\x35\x5a\x2c\x00\xa1\x29\x26\x62\x09\x56\x54\xbf\xb3\xe0\x4d\xbf\xe9\xe9\xf6\x14\xaa\x05\x11\x74\x3c\x75\x18\xb4\x0b\xcd\x89\x38\x01\xd7\x90\x5f\x30\x94\xe0\xef\x20\xd0\xc9\x37\xa5\x49\x4e\xee\x6d\x06\xa6\xcf\xa7\xed\xe9\x6d\xf5\x5a\xf6\xa8\xdd\xbc\xb5\xb0\xed\x72\x0d\x43\x7c\xea\x0d\xd2\x4d\xcf\xf4\x34\x17\x8e\x57\x9c\x29\xb7\xd8\x87\xe7\x85\x7d\x75\x96\xd4\x17\x67\x46\x51\xac\x40\xe9\x16\x8b\xe8\x1a\x24\xfb\x12\x53\x04\xb9\x7e\xd7\x51\xee\xda\xe9\xd2\xe9\xdf\x83\x28\x35\x2b\xda\x6c\xde\x16\x83\xf9\x08\x75\x10\xf6\xd3\xc1\x60\x71\x0f\x02\x2e\xe3\xbe\xcc\x72\x02\x1b\x89\x68\x6d\xad\x4c\x8b\x25\x15\xa4\x5b\xb4\xdd\x82\x75\xfb\x1a\xb2\x6f\x28\x96\x21\xdd\x57\x54\xa6\xb8\xbf\xb6\xcc\x7d\xd9\xe5\x9f\xa3\x69\x61\x10\x54\x30\x2c\xdb\x53\x75\x97\xe9\x75\x36\x03\x81\x0c\xc4\xaa\x0c\x05\x18\x13\x6f\x35\x02\x2b\x3b\x67\x3f\x90\x1f\x28\x74\x46\x04\x9c\x58\x64\x9a\xa9\xc6\x15\xea\x9c\xb4\x25\x64\x94\x7e\xd2\x6b\x49\x45\x12\x4c\x73\xa2\x64\x23\x68\xb9\x82\xf5\xc6\xe9\x59\x07\xe8\x67\x6a\x67\x3e\x1b\x3c\x54\x9f\x81\xe0\x67\x3e\x5b\x82\x9f\xf9\xb4\xe9\x6a\x29\x72\x5a\x19\x0a\xdf\x10\x4e\x45\x0e\x4d\xf5\x89\xd1\x7d\xd4\xc7\xcc\x1e\xa1\x3e\x2d\x0c\xfe\x5a\xea\x63\xd0\xdf\xbb\xfa\x18\x46\x3e\x64\xf5\xb1\x7b\xa5\x2e\x5d\x40\x79\x7e\xc0\x2c\x4b\xf5\xcb\x1c\x42\xd5\xd0\x3a\x29\x0c\x81\xc7\x45\x6b\xf9\xc2\x65\xe4\x35\x82\x5a\x3c\x30\x6e\xda\x90\xcb\x63\xe5\x8e\x8f\xf5\x6d\x0b\xaf\x1f\x47\xfa\x6a\x8c\x99\x57\x6b\xcb\x4f\x7a\x65\x3b\x34\x3a\xe7\xaf\xff\xc8\x61\x1a\xd8\xf1\xd2\xcc\x12\x7f\x06\x09\x8e\x82\x69\x04\x89\xf4\x47\x33\xc5\xbc\x84\xd1\x35\x80\x40\x53\x71\x8b\xc5\x35\x88\x71\x92\x20\x86\x88\xa8\x1e\x6e\x4d\x9d\xab\x68\x4e\xd5\xed\x97\x5e\xdd\xef\x22\x7b\xe2\x1e\xeb\x2d\x5a\x78\x7f\x66\xd5\x55\xe0\x7b\x9c\xde\xfd\xd9\xaa\x2d\xc7\x76\xd7\x71\xdd\x0b\xec\xb9\x17\x34\x3b\xc9\xd0\x6a\x1b\xba\x12\x38\x43\x28\x6b\xbc\x51\x8b\x11\xca\xf4\xb3\x2c\xbc\xe5\x59\x96\x7a\x4e\xea\x6d\x23\xf5\x42\xbe\x97\xb0\x6e\x82\xf5\xc9\x13\x6b\xdf\x3e\x29\x26\x93\x27\xe6\xf9\xc5\xf0\x25\x6d\x31\x79\x42\xc3\x72\xe5\x73\x22\x68\x40\x73\x31\x9b\x4c\x9e\x74\x3c\x02\xaa\x07\x49\xe2\x31\xe2\x6e\x4c\x89\x89\xd9\xd4\xfa\x90\x18\xa4\x61\x34\x53\x4a\xd4\xb6\x8d\xdf\x4c\x26\x4f\x04\x64\x2b\x24\xe6\x65\x6a\xd8\x79\xc3\x1d\x2a\x0e\xd3\xd9\xe4\x89\xc9\x1d\xff\x54\x33\x50\xef\x55\x27\x21\xf2\x4f\xcb\x54\xa3\x4c\x89\x7c\x68\x7d\x63\x7f\xa5\xbd\x9d\x69\x21\x3c\xd7\x0f\xd5\x9e\x6b\x9c\x86\x1e\xa8\xa9\x5d\x6b\x1e\x9a\xe8\xf7\xe0\xea\xa6\x0d\xc7\x86\xcf\x51\xce\xb4\x85\x20\x09\x65\x6b\x9d\xba\xe2\x3a\x01\x5d\x71\xbe\x26\xd3\x3b\xbf\x6a\x96\x0a\x1a\xd9\x7b\xf5\x87\xb2\x99\xb5\x99\x55\xe7\x17\xdf\x94\x17\x8d\x7f\xe4\x98\xa1\xf8\xf5\xd0\xc0\x1d\xcf\xeb\x2a\xf1\xf5\x89\x41\xc2\xb1\xa4\xda\xe9\x0b\x5f\x7f\xcf\x28\x47\xf5\x91\x65\x9a\x3f\x1a\x9c\xdc\xd1\xe8\x0f\xa0\x1f\x6e\x4f\x75\xb0\x3c\xb5\xac\xa0\x51\x91\x1a\xf5\x92\x1f\x25\x28\x73\xe4\x3b\x11\xce\xbc\xc7\x16\xf5\xbb\x00\x0e\xab\x4e\x1a\x0d\xa1\x79\x7c\xd9\x3a\xa5\x9d\x53\x59\xd3\x42\x19\x08\x6a\x7a\x10\xc9\xd7\xd3\xd9\xfd\xc9\xe1\xbd\x7e\x8d\xa4\xea\x07\x90\x55\x93\x24\xf0\x1a\x8d\x12\xd0\x27\xbc\x46\x0f\x55\x3c\xdf\x05\x62\x04\xa6\xd3\x99\xdd\x9a\x62\x2e\xa6\xb3\x11\x14\xbe\x36\x60\x1e\x0c\x95\x35\x2d\x98\x08\xb4\x42\x6c\x94\xc0\xce\x89\x78\x80\x94\x24\x29\x85\x62\x14\x1d\x6f\xe4\x8c\x87\x41\xc9\x10\x61\x0c\x25\x53\xaf\xfb\x74\x17\xb5\x9a\xee\x8f\x88\x23\x61\xae\x74\xde\x50\xf6\x3f\x88\x51\xfd\x7d\xcd\xd6\xec\x48\xcd\xc5\xee\x91\x61\x7d\xf8\xf4\x30\xc8\x3a\x89\x4e\xcc\x2f\x2d\xa6\x00\x2b\xab\xe2\xb9\x31\x19\x4a\xde\xaa\x5d\xd8\x68\x7c\x07\xb3\xda\x9c\x9a\x64\x1a\xcf\xaf\xac\x77\xe8\xdd\xdc\xdb\xd8\x24\xcb\x09\xad\x6b\x6f\xa0\xbe\x22\x20\x02\x93\x1c\x35\xb0\xf6\xe5\x36\xcf\xaf\xba\x58\xcb\xf3\xab\x1f\xc8\xc7\xf0\x65\x9a\xd2\x5b\x14\x9f\x5e\x53\x1c\x21\xee\xb3\x5f\xf4\x91\x73\x4e\xd4\x9b\xf7\x51\x07\xcf\xbc\xbe\x6a\x94\xf3\x7e\xa7\x98\xb4\x10\xf8\x3a\x9d\x83\xe9\x57\x09\xad\x98\x2b\xd7\xec\x65\x2e\x68\xfd\xa9\x62\xff\xde\xdb\xc6\x0e\x2f\x26\x40\xe6\xc5\x82\x0b\x28\xa4\x09\xf7\x33\x16\x73\x75\x7f\xd8\x5c\xe3\x6b\x47\xf3\x3b\xc4\x39\x5c\x21\xdd\x2b\x3b\x2d\xff\xe7\x00\x64\xaf\x04\x08\xdf\xc1\xef\x6f\x11\x59\x89\x6b\xf0\xc2\x87\xf0\x77\xf0\x3b\x5e\xe7\x6b\x3d\xc5\x97\x7c\xd9\x5a\xaf\x23\x5b\x54\x7c\x79\x28\x8a\x30\x19\x45\x11\x26\x3b\x52\x54\xad\x73\x70\x8a\xe0\x77\x65\x32\xc0\x8b\xf0\x45\x9f\x27\xec\x7f\xdc\x19\x11\x8e\x38\xed\x2a\x09\xfe\x66\x3e\x8f\xdc\x1b\xb9\x26\x23\xe0\x8b\xb3\xb7\xa7\x31\x97\x11\x54\xd0\xc0\x7a\xb6\x67\x29\x6d\xd3\xc2\x7d\xca\x4c\x2b\xe9\x78\x99\x95\x58\xec\x5f\x66\x9e\x28\xef\x22\xb2\x1a\xe9\x1f\x23\xb2\xea\x35\x7a\x08\x5a\xdf\x73\xab\xd7\xea\xea\x7b\xea\xa1\x8f\xba\x6b\x46\x48\x70\xeb\x92\x58\x45\xb9\x7e\x80\xde\x20\xbf\xeb\x55\xfa\xfd\x6d\xfc\x71\x8f\x1f\xe9\xc1\x04\x87\xde\x9b\x8a\x52\x85\x58\x95\x67\xd5\x09\x87\x9a\x0f\xf6\x17\xd9\xa7\x39\x17\x74\x5d\x5e\xa2\xd7\x10\xc2\x3a\x6b\xbb\x86\x59\x86\xc9\x4a\x7d\xd6\xad\xde\x79\xd5\x90\xde\xe9\xae\xd0\xfc\x1f\x4c\xeb\x2f\xfe\x5b\xe8\x34\x72\xba\x25\xd4\x6e\x51\x18\xb8\xa5\x40\xe8\xde\x58\xdc\xc5\x71\x73\x1f\xef\x3a\xfd\x33\xf0\x0f\xeb\x5a\xde\x64\xe1\xdc\x21\x66\x05\x1b\x06\xea\x98\x6b\xbf\x76\x6c\xcc\x6a\xdd\x10\x49\x79\xe1\x04\x47\x8a\x6d\x6f\x28\xab\x92\x34\xce\xfb\x88\xaa\xd5\x19\x5e\x3d\xa0\xd2\x79\xbf\xfa\x2e\x43\x7d\x3e\xff\x0d\xdd\x95\xc9\xa8\x6d\xef\x94\xfa\x70\x08\x14\xa0\xf6\x4b\x87\x1e\x74\xea\xf4\xe8\xcd\x1c\xd0\x6f\x46\xb6\xbd\x0b\xd7\xf9\xa8\x77\x30\xfb\x2c\x97\xfa\xf2\x8b\x9c\xd6\x62\xe3\x8d\xcd\xc1\xc5\x02\xfc\x0b\x81\x88\xe6\x69\xac\x52\x55\x09\x26\x31\xc0\x62\x0e\x38\x05\x29\x12\xcf\x38\x88\xae\x51\xf4\x0d\x50\xf3\xf9\x1e\xbd\x45\x4c\x5f\x96\x63\x12\xa3\xef\x28\x06\x3c\x43\x11\x58\xc3\xcc\x16\xc8\x10\x9e\x6f\x25\x88\x53\xc8\x51\x07\xc2\xe5\x17\xc5\x9d\x0c\xe1\x8e\x0c\x93\x3c\x4d\x2d\x19\x71\x77\xe4\x1a\x66\x9e\xd2\xea\x59\x2b\x98\x49\x18\x9f\xb5\xb0\xbe\xf8\xca\xca\x83\x7c\x87\xea\x3a\x4f\x9a\xa3\x5e\x6d\xd5\x37\x52\x3d\xca\xe9\x3c\x97\x86\xe0\x06\xb1\x3b\x00\xe3\x1b\x48\x22\x14\x03\xc9\x00\x85\x9e\xb8\x86\x02\xdc\xd1\xdc\x3c\xd4\x52\x92\x26\x08\xc5\xe0\x2a\x17\x00\x13\xc0\xe9\x1a\x49\x40\x6a\x7a\xc9\x4a\x90\x73\xa4\x44\xed\xf7\xf2\xd2\x64\x61\x5d\x42\x5c\x95\xb7\x1e\xfb\x97\x1c\x33\xef\x38\xd4\xb0\x4d\xc3\x28\x7b\xe6\x59\x87\x9e\x6e\x0c\xbf\x64\xeb\xb0\x6d\x7d\x86\xc5\x5b\xa4\xad\x4f\x0e\x61\xa6\x6e\x13\x2b\xd1\x4a\x41\x0e\x5f\x29\x6c\xab\x7b\xe1\xae\x77\x32\x46\x51\x37\xcd\x83\x6f\x54\x2e\xdb\xfa\xe4\xac\x9a\x21\x51\x68\xbc\xf4\x3b\xb6\x6b\xbe\x34\x99\x3e\x5d\xb6\xef\xe4\x3a\x03\x51\xf9\x63\xb7\x2f\xdb\x81\xa3\x0c\x18\x1d\x58\x8d\x47\x2a\x6e\x8c\xbd\xec\x89\xfd\x8f\x8b\x62\x54\x7c\x5e\x7b\x83\xd5\xb4\xa2\x72\x2c\xe6\x6d\xda\x1a\x81\x7c\x8d\x9d\xdd\xb1\xec\x0c\xfa\x07\xa9\xeb\x7e\xbd\x2f\x7f\x5e\x5d\x7e\x78\xaf\x3e\xee\x7c\xaf\xaa\x9f\x4c\xcd\x17\x9f\x4e\x73\xfb\x01\x73\xef\x02\xbd\x17\x92\x4e\xc7\xb2\x47\xde\x7e\x4b\x30\xa4\xf4\xf3\x03\x49\xef\x9c\x15\xac\x76\xcd\xa2\xc6\x48\x2f\xe8\x26\x0f\x55\xba\xd7\x55\xbf\xdd\xae\xa0\xab\xaf\x1d\x9d\xd1\xce\xe7\x8e\xa1\xff\x82\x19\x43\x51\x53\xe0\x75\xab\x26\xc5\x19\xe5\x09\xd7\x79\x12\x5e\x03\xae\x9a\x97\x1d\xaf\xb6\x1b\x4f\xb5\xbd\x56\x6a\x3d\x32\x91\x3f\x55\xa3\xc6\xdf\x1e\xe3\x07\xb4\xbe\xee\xaa\x40\xea\x26\x03\xb0\xea\xf7\x02\xf7\x06\xa7\x02\xb1\xf2\x79\x59\xd9\x5b\xb7\x6a\xa0\xce\x28\x3f\xb8\x94\x21\xbc\x22\xff\x8d\x1c\x55\xac\x5b\x0d\x5c\x7b\x94\x17\x5c\xf3\x34\xdc\xea\xd1\x2d\x1a\x5e\xd5\xeb\x05\xab\xfd\x71\x8e\xfc\xa9\x5b\x35\x4c\x67\x94\x17\x5c\x3b\x61\x55\x75\x56\x8d\xcb\x76\x52\xcb\x13\x68\x6b\xef\x95\x6d\xcb\x56\x96\xc5\x0b\xa2\x95\x85\xaa\x41\x96\x8d\xcb\x76\xa6\xca\x13\x68\x1b\x4d\xd3\xb6\x6c\x25\x16\x7c\x20\x36\x0d\xa6\x65\x27\x47\x99\x47\xf5\x09\x4a\x53\xd1\xab\x46\x8d\x9b\x3d\xc6\x0b\xe8\x05\xc3\x6b\xc8\xee\x1a\x6a\x5e\xb7\x6a\xb0\xce\x28\x2f\xb8\x1f\x11\x8c\x9b\x76\xbc\x6c\x5b\x9a\xfc\x6e\x35\xc2\x13\xa2\x7b\x1f\xae\x21\xea\xb6\x65\x33\x63\xec\x77\x66\xa2\x88\x21\xe7\x6b\x74\xdd\x52\x7e\xd6\x6f\x7a\x3d\x61\x35\xb7\xf5\xa5\xb5\xad\x2f\x47\x6d\xeb\x4b\xbc\x22\x2e\x9d\xba\xc5\xc0\x2a\x7b\x77\xf4\x0b\x74\x8b\x81\x55\xf6\xfa\xc1\xca\xaf\xcc\xa7\x0f\x35\x30\xdd\x54\x56\x4b\xab\x06\xf8\x69\x74\xeb\x45\x84\xfc\xa9\x1a\x35\x8a\xf6\x18\x3f\xa0\x0d\x14\x2d\xfc\xb6\x22\x67\x56\xe8\x4f\x75\x6c\x8f\x03\xba\x23\xdb\x1f\x10\x10\xf4\x2c\xfc\xb0\x23\x03\x9d\x4a\x08\x1f\xc3\x82\xc7\xb0\xe0\x31\x2c\x78\x0c\x0b\x1e\xc3\x82\xc7\xb0\xe0\x31\x2c\x78\x0c\x0b\x1e\xc3\x82\xc7\xb0\xe0\x2f\x16\x16\x6c\xee\xfd\x21\xb7\xbe\xba\xf4\xaf\x2c\xd9\x5d\xb3\xd9\x17\x82\x2e\x4a\x38\x6a\xbd\xcf\x5f\xb6\x7d\xac\xb3\xb7\x2a\xce\x63\xf0\xfa\x53\x6b\x39\x8f\x2b\x85\xba\x1b\x79\x3b\xd5\x75\x1e\xb3\xc4\x41\xaa\x3b\xff\x08\xce\x1c\xa8\xd2\xf3\xee\xbc\xbb\x5f\xbd\xe7\xad\xbb\xeb\x07\x54\x7d\x1e\x27\x80\xff\xd4\xda\xcf\xe3\xb8\xf4\x20\x4a\x11\xe9\x42\x21\x17\x29\xc4\x6e\x6d\xab\x51\x47\x80\x53\x06\xfa\xb0\x7b\xdb\xe0\xfa\xb0\xb4\x2b\xac\xb0\x1a\xd4\xb3\x5d\x8a\x2b\x8f\xe3\xce\xee\x25\x96\xb7\x7b\x19\x1d\x35\x93\xdb\xc5\x64\x86\x8a\x27\x87\xfb\xaf\x9e\xec\x81\x72\x55\x43\xb9\x63\xeb\xf8\x94\xc1\xeb\x28\x88\xac\xea\x26\x87\x1d\x0e\x9f\xfa\x7b\x3f\x35\x91\xbd\x84\x61\x57\x46\xf6\xe0\x45\x58\x15\x48\xde\x3e\x36\x98\x79\x7d\xe3\xbe\x71\x7c\x83\xed\x13\x36\xa5\xb2\x79\x55\xe0\x35\xfa\xd5\xfa\x82\xde\xe3\xe3\xf0\x43\x94\xe3\xf5\x2d\xb0\xeb\xa0\xbd\xa5\x6c\xac\x3f\x29\xbb\x54\xdb\xad\xbe\x86\xda\x5e\x3a\xb6\xa7\xa8\x83\xa9\xb2\x5b\xd8\xec\x7a\x7e\xa0\xea\xbd\xbe\xf5\x78\xf7\xcd\xdf\x7b\x16\xe7\xc5\x09\xc0\x71\xab\x5c\xab\x77\xc9\xde\xa7\xa6\x66\x6f\xa1\xf9\xb4\x03\x84\xba\xd2\x68\x27\x4b\xff\xec\x82\xbf\x3e\x22\xf8\x91\x65\x7f\xbd\xac\xd4\x8e\xa7\xc5\x1e\x8b\xff\xba\x44\xfa\x57\xff\x7d\x3e\xbe\xfc\x6f\x5f\x95\x18\x17\x8d\x5d\x8a\x04\xb7\x54\x72\xf8\x4f\xdb\x42\x3c\xa8\x52\xc1\x9e\x66\xe4\xe0\x05\x83\xbd\x75\xf7\x2f\x52\x36\x18\xc7\xea\x03\xcf\x6d\x35\x82\x7b\x0b\xa3\x3c\xb5\x0b\x9e\xbb\x1a\xdd\x01\xb4\xa9\xd1\xbb\x94\x1a\xbe\x87\x46\xd7\xea\x7c\xaf\x6a\xc2\x3e\xba\x78\xaf\x40\xa0\x2b\x1e\xcc\x54\x4b\x03\x33\x73\x10\xef\x82\x60\x57\x1c\xa7\x5a\xba\xaa\x3a\x9a\x38\x6e\xeb\xbf\xec\x51\x31\x7f\xd4\x33\x85\x6d\xd6\x6f\x4f\x85\x82\x72\xb1\x83\xfd\xec\xd2\xed\x2d\xd0\x7a\xec\x7f\x6f\x16\x7b\x48\x63\x4d\xec\xba\xf9\x93\xeb\xf6\xf9\x28\xd4\x43\xad\xde\xd7\x25\x8e\xed\xe5\xfb\xba\x67\x1d\xbe\x7e\x5f\xc5\xe9\xff\xbf\x55\xfc\x7c\x94\xe9\xa1\xd6\xf2\xf3\x55\x26\xb7\x98\xdf\x1e\x95\x69\x54\x35\xbf\x3f\x51\x99\x36\x7f\xdd\xb2\xdd\x1e\x5c\x1b\x2a\xde\xad\x2a\x88\x6d\x86\x0a\x53\xf7\x47\x1d\x3d\x9e\x69\x47\x52\xb3\xef\x90\x79\x98\x05\xc0\x61\x1c\x33\xc4\xab\xbc\x7d\x77\x3d\x70\x2f\xbe\x1f\xae\x2a\xf8\xd3\xcd\xf6\xb2\xe0\x9e\xa5\xfa\xbc\x33\x81\xfe\xe6\xb0\x2e\xdb\xe7\x95\x14\xec\x08\x76\xfa\x8b\xf7\x79\xc5\x34\x07\x2b\xe1\x77\x30\x66\xd5\xe5\xfc\x7c\x66\x1d\xbe\xa8\xdf\x76\x2c\x3c\x4a\xfb\xf9\x94\xe5\x74\x54\x56\xa5\xe9\x47\xfc\xab\x37\xcd\x7c\xfd\x88\x7f\xf7\x70\x30\x00\x35\x69\x7c\x15\xe6\xf7\xfc\xe3\x86\xc3\x67\xcc\x5e\x1e\x73\x74\x71\x63\xfc\x1d\xc6\xa1\x79\xd2\x7b\xbf\xd1\x62\x89\xf5\xc7\xff\x05\x00\x00\xff\xff\xeb\x01\x0f\x31\x44\x81\x00\x00")

func templatesModelGotplBytes() ([]byte, error) {
	return bindataRead(
		_templatesModelGotpl,
		"templates/model.gotpl",
	)
}

func templatesModelGotpl() (*asset, error) {
	bytes, err := templatesModelGotplBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "templates/model.gotpl", size: 33092, mode: os.FileMode(420), modTime: time.Unix(1664490134, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var _templatesRelationships_registryGotpl = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xec\x98\x5f\x6f\xdb\x36\x10\xc0\xdf\xf5\x29\x0e\x86\x31\x24\x83\x27\x2f\xe9\x9b\x81\x3c\x0c\x71\x51\xe4\x21\x69\x90\x66\x7b\x09\x82\x81\xb5\xce\x0a\x37\x9a\x62\x69\x2a\x85\x21\xe8\xbb\x0f\xb4\x24\x5b\x96\x49\x51\xdd\xe6\x61\x29\xee\x0c\x18\x10\x79\xff\x48\xde\x51\x3f\x68\x3a\x85\xeb\x2c\x41\x48\x51\xa2\x66\x06\x13\xf8\xbc\x01\x14\x98\xa2\x8c\x61\xfe\x11\xee\x3e\x3e\xc2\xfb\xf9\xcd\x63\x1c\x4d\xa7\xf0\x29\xcb\xf5\x02\x67\x90\x66\x31\x53\x99\x46\x93\xc5\x3c\x9b\xa2\xc0\x15\x4a\xc3\x04\x9c\x19\x5c\x29\xc1\x0c\xae\xa7\x1a\x05\x33\x3c\x93\xeb\x17\xae\xd6\xbf\x6b\x4c\xf9\xda\xe8\x4d\x9c\x66\x46\x89\xf3\x28\x52\x6c\xf1\x27\x4b\x11\x8a\x02\xe2\x4f\x68\xe2\xeb\x4c\x2e\x79\x9a\xeb\xad\x4d\x7c\xc7\x56\x08\x65\x19\x45\x7c\xa5\x32\x6d\x60\xe4\x09\x38\x8a\xa2\x57\xa6\xe1\x20\xd6\x43\x1d\x0a\x76\x5a\xf1\x83\x6b\x3e\x8a\x96\xb9\x5c\x00\x97\xdc\x9c\x9d\x43\x11\x45\x00\xe0\xf1\x74\x15\xf2\x55\x94\x95\x79\x51\x80\x66\x32\x45\x18\xa3\x34\xdc\x6c\xec\x3a\x26\x30\x6e\xbc\xc2\xec\xaa\x5a\xed\x81\x13\xbb\xd0\xca\xf8\x27\xe0\x4b\x58\xbf\x64\xb9\x48\x2a\xcf\xa8\xdb\x9a\x30\xb6\xc6\x6d\xdf\x30\x8e\xef\xf3\xcf\x82\x2f\x6e\xb3\x04\x6b\x37\xce\x25\x3c\x15\xc5\x81\x5d\x59\xde\x24\xd5\xe3\x33\x5c\xc1\x0f\xee\xe5\x15\x5b\x7f\xad\xd4\x52\x03\x67\x02\xe5\x7e\x41\xf1\xb5\x46\x66\xf0\x1c\x7e\x6e\x16\x61\xa5\x1a\x9c\xc1\x8a\xa9\xa7\xb5\xd1\x5c\xa6\xcf\x3f\xba\x23\xdc\xc8\x65\x06\xfb\x30\x4d\xa8\x7a\x0f\x15\xd3\x28\xcd\x04\xc6\x6c\xd1\xec\x5e\x37\x72\x3b\xac\x77\x0f\x6f\xa4\x74\x6f\x64\x15\xe0\x60\x13\xbb\x0e\x47\x76\xe3\x6a\xbd\xb2\x1c\xc1\xec\x30\xdb\x56\xcc\x3a\xcb\x78\x8e\x4a\xe3\x62\xdb\x49\x1d\x5f\x56\xf6\xb3\x33\x30\x3a\xc7\x89\xd3\x1d\x4a\xa7\x71\x27\xd2\x3d\xd3\x6c\x85\x06\xf5\x1c\x97\xb6\x8c\xed\x1e\xf9\xad\x76\x87\xe7\xb7\x8e\x1f\xf0\x4b\xce\x35\x26\x9d\x03\x6d\xa4\x99\xde\x99\xae\x67\xad\xce\xb8\xc3\xaf\xfb\x89\x5a\xd5\x4e\x9d\x1d\xf9\xb1\xf2\xf4\x6c\x7f\x55\x79\x1c\xef\x69\x3b\xf7\xba\x1a\xf8\x04\xc6\xe2\x62\x5b\x04\x03\x56\xe0\x4a\xdf\xb7\x21\xe2\xc2\xb3\xde\x9d\x81\x77\xc6\x95\xe3\xe5\x36\x47\x71\xd1\xe7\xd1\x99\xc6\x65\x20\x8d\x70\x2a\xae\x74\xde\x55\xe9\x5c\x86\x3c\x43\x53\xed\xe2\x9d\xad\xf4\xe3\xc2\x74\x05\x42\x99\x04\xdc\x96\xfd\x8e\x86\x39\x09\x6b\xf5\x84\x09\x1b\xf7\x6b\x38\x5c\x9f\xff\x9d\xb6\x1d\xd2\x80\xef\xa5\xd1\x1c\xd7\x9e\x42\x68\xf7\xdd\xd3\xf3\xbe\xf3\x1c\x9e\xdc\xd7\x54\xab\x2e\x54\xa8\x93\xea\x54\xbc\x65\xe3\xaf\x44\xfb\x86\x99\xd5\x37\x67\xf3\x2a\xef\xa9\xa7\xc7\x8d\xda\xab\xdb\x87\x7e\xf5\xe6\x12\x54\xf1\x1c\x97\x2c\x17\xe6\x37\x26\xf2\xa3\x7b\xbb\x2d\x6d\xbd\x5d\xa0\x8e\x71\x20\x60\xb8\x7e\xf8\x12\xf0\xcb\x6e\x05\x23\x94\xf9\x6a\xd4\x97\xd4\x2f\x42\x64\x5f\x31\xb9\x7e\xc9\xf8\x02\xb7\xe7\x19\xba\x0b\xe1\xf0\x10\xff\x98\xc0\xf8\x75\x7b\x88\x2a\x3e\x74\x16\xea\xf4\xed\x0e\xbc\x86\x9b\xbc\xa7\xa4\x1b\x09\xb7\xdd\x80\x8b\x78\xac\xe2\xdb\x5c\x18\xae\x44\xef\x31\x36\x3a\xbe\x17\xe7\xc0\xc0\x8e\x94\x7b\x2c\xbe\x4d\xdb\x33\xd5\x71\xe2\xd1\x72\x0c\xb7\x0c\x1d\xb3\x5e\x2a\xfb\x55\x25\xc7\x54\x56\x0d\x9e\x98\xca\xaa\x20\xff\x3d\x95\x39\x6e\x23\xc2\x32\xc2\x32\xc2\x32\x9f\x10\x96\x7d\x9f\x58\x46\x54\xd6\x4d\x89\xa8\xec\x1b\xad\x89\xca\x02\xc3\x2d\xc3\x7b\x66\x16\x2f\xc4\x54\xc4\x54\xc4\x54\x21\x3b\x62\x2a\x62\x2a\x62\x2a\x62\x2a\x62\x2a\x62\xaa\x7f\xed\x4b\xd7\x1c\x05\x1e\x7d\xe9\xaa\x06\x4f\x4c\x65\x55\x10\xa2\x32\xa2\x32\xa2\xb2\xfe\x74\x88\xca\x88\xca\x80\xa8\x8c\xa8\xac\x11\xa2\xb2\xe1\x16\x6f\x92\xca\x3e\xa0\xe9\x5c\x20\x0f\x68\x7b\xf9\xf5\xd4\x50\xf6\x01\x0d\x11\x19\x11\x19\x11\x59\x7f\x3a\x44\x64\x44\x64\x40\x44\x46\x44\xd6\x08\x11\xd9\x70\x8b\xb7\x4a\x64\xb7\x4c\x6e\x3c\x54\x66\xa7\x4e\x4f\x66\x36\x0a\xd1\x19\xd1\x19\xd1\x59\x7f\x3a\x44\x67\x44\x67\x40\x74\x46\x74\xd6\x08\xd1\xd9\x70\x8b\xff\x0d\x9d\x59\x56\x22\xa6\x22\xa6\x22\xa6\x22\xa6\x22\xa6\x22\xa6\x22\xa6\x3a\x16\x62\x2a\x62\xaa\x7f\xf0\xc5\xab\xfa\xef\x0c\x16\x45\xf3\x54\x46\x7f\x05\x00\x00\xff\xff\xd6\x12\xad\xdf\xdc\x44\x00\x00")

func templatesRelationships_registryGotplBytes() ([]byte, error) {
	return bindataRead(
		_templatesRelationships_registryGotpl,
		"templates/relationships_registry.gotpl",
	)
}

func templatesRelationships_registryGotpl() (*asset, error) {
	bytes, err := templatesRelationships_registryGotplBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "templates/relationships_registry.gotpl", size: 17628, mode: os.FileMode(420), modTime: time.Unix(1648157562, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"templates/README.md": templatesReadmeMd,
	"templates/identities_registry.gotpl": templatesIdentities_registryGotpl,
	"templates/model.gotpl": templatesModelGotpl,
	"templates/relationships_registry.gotpl": templatesRelationships_registryGotpl,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}
var _bintree = &bintree{nil, map[string]*bintree{
	"templates": {nil, map[string]*bintree{
		"README.md": {templatesReadmeMd, map[string]*bintree{}},
		"identities_registry.gotpl": {templatesIdentities_registryGotpl, map[string]*bintree{}},
		"model.gotpl": {templatesModelGotpl, map[string]*bintree{}},
		"relationships_registry.gotpl": {templatesRelationships_registryGotpl, map[string]*bintree{}},
	}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	return os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}

