module go.aporeto.io/elemental

go 1.13

require go.aporeto.io/regolithe v1.50.1-0.20220308204532-35cc3143a6b4

require (
	github.com/araddon/dateparse v0.0.0-20200409225146-d820a6159ab1
	github.com/getkin/kin-openapi v0.65.0
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-test/deep v1.0.7
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/golang/mock v1.6.0
	github.com/mitchellh/copystructure v1.2.0
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/ugorji/go/codec v1.2.7
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb // indirect
	go.uber.org/zap v1.19.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/text v0.3.7
	golang.org/x/tools v0.1.1
	gopkg.in/yaml.v2 v2.4.0
)
